; ; ;   T M P 
 . e q u   S E G _ A ,   0 x 8 0   
 . e q u   S E G _ B ,   0 x 4 0   
 . e q u   S E G _ C ,   0 x 2 0   
 . e q u   S E G _ D ,   0 x 0 8 
 . e q u   S E G _ E ,   0 x 0 4 
 . e q u   S E G _ F ,   0 x 0 2 
 . e q u   S E G _ G ,   0 x 0 1 
 
 ; ; ;   M I S C 
 . e q u   L E F T _ L E D ,   0 x 0 2 
 
 ; ; ;   N U M B E R S 
 
 . e q u   N U M _ K E Y _ 1 ,   0 x 0 1   @ b u t t o n ( 0 ) 
 . e q u   N U M _ K E Y _ 2 ,   0 x 0 2   @ b u t t o n ( 1 ) 
 . e q u   N U M _ K E Y _ 3 ,   0 x 0 4   @ b u t t o n ( 2 ) 
 . e q u   N U M _ K E Y _ 4 ,   0 x 1 0   @ b u t t o n ( 4 ) 
 . e q u   N U M _ K E Y _ 5 ,   0 x 2 0   @ b u t t o n ( 5 ) 
 . e q u   N U M _ K E Y _ 6 ,   0 x 4 0   @ b u t t o n ( 6 ) 
 . e q u   N U M _ K E Y _ 7 ,   1 < < 8   @ b u t t o n ( 8 )   -   d i f f e r e n t   w a y   t o   s e t 
 . e q u   N U M _ K E Y _ 8 ,   1 < < 9   @ b u t t o n ( 9 ) 
 . e q u   N U M _ K E Y _ 9 ,   1 < < 1 0   @ b u t t o n ( 1 0 ) 
 . e q u   N U M _ K E Y _ 0 ,     1 < < 1 3   @ b u t t o n ( 1 3 ) 
 
 
 . e q u   N U L L _ K E Y ,       1 < < 1 2   @ b u t t o n ( 1 2 ) 
 . e q u   E N T E R _ K E Y ,     1 < < 1 4   @ b u t t o n ( 1 4 ) 
 
 ; ; ;   O P E R A T O R S 
 . e q u   P L U S _ K E Y ,     0 x 0 8   @   A D D 
 . e q u   M I N U S _ K E Y ,   0 x 8 0   @   S U B S T R A C T 
 . e q u   M U L _ K E Y ,       1 < < 1 1   @   M U L T I P L Y 
 . e q u   D I V _ K E Y ,       1 < < 1 5   @   D I V I D E 
 
 m a i n :       m o v   r 4 ,   # 0   @   p o s i � � o   d o   c u r s o r   g e r a l   d a   t e l a   L C D 
                 m o v   r 1 0 ,   # 0   @   p o s i � � o   d o   c u r s o r   d o   o p e r a n d o 
                 m o v   r 5 ,   # 0   @   o p e r a t o r s   p l a c h o l d e r 
 
                 m o v   r 6 ,   # 0   @   o p e r a n d s   p l a c h o l d e r 
                 m o v   r 8 ,   # 0   @   o p e r a n d s   p l a c h o l d e r 
 
                 l d r   r 9 ,   = o p e r a n d s   @   v e t o r   d e   o p e r a n d o s 
 
                 m o v   r 7 ,   # 1 0 
 
 w a i t i n p u t : 
         s w i   0 x 2 0 3 
         c m p   r 0 ,   # 0 
         b e q   w a i t i n p u t 
 
         c m p   r 0 ,   # P L U S _ K E Y 
         c m p n e   r 0 ,   # M I N U S _ K E Y 
         c m p n e   r 0 ,   # M U L _ K E Y 
         c m p n e   r 0 ,   # D I V _ K E Y 
         b e q   p r e s s e d o p e r a t o r 
 
         c m p   r 0 ,   # E N T E R _ K E Y 
         b e q   p r e s s e d e n t e r 
 
         c m p   r 0 ,   # N U L L _ K E Y 
         b e q   w a i t i n p u t   @   i f   " N U L L "   k e y ,   i e   3 . 0 ,   p r e s s e d ,   b r a n c h   t o   i n p u t   l o o p 
 
         b   p r e s s e d n u m b e r   @   i f   n o n e   o f   t h e   a b o v e ,   d e f a u l t s   t o   " n u m b e r " 
 
 
 d r a w :   A D D   r 0 ,   r 0 ,   # 1 
         S W I   0 x 2 0 5 
         B   w a i t i n p u t 
 
 
 p r e s s e d n u m b e r : 
         c m p   r 0 ,   # N U M _ K E Y _ 1 
         m o v e q   r 2 ,   # 1 
 
         c m p   r 0 ,   # N U M _ K E Y _ 2 
         m o v e q   r 2 ,   # 2 
 
         c m p   r 0 ,   # N U M _ K E Y _ 3 
         m o v e q   r 2 ,   # 3 
 
         c m p   r 0 ,   # N U M _ K E Y _ 4 
         m o v e q   r 2 ,   # 4 
 
         c m p   r 0 ,   # N U M _ K E Y _ 5 
         m o v e q   r 2 ,   # 5 
 
         c m p   r 0 ,   # N U M _ K E Y _ 6 
         m o v e q   r 2 ,   # 6 
 
         c m p   r 0 ,   # N U M _ K E Y _ 7 
         m o v e q   r 2 ,   # 7 
 
         c m p   r 0 ,   # N U M _ K E Y _ 8 
         m o v e q   r 2 ,   # 8 
 
         c m p   r 0 ,   # N U M _ K E Y _ 9 
         m o v e q   r 2 ,   # 9 
 
         c m p   r 0 ,   # N U M _ K E Y _ 0 
         m o v e q   r 2 ,   # 0 
 
         c m p   r 4 ,   # 0 
         m u l g t   r 8 ,   r 6 ,   r 7   @   m u l t i p l i c a   o   n � m e r o   d o   o p e r a n d o   a n t e r i o r   p o r   d e z   e   g u a r d a   e m   R 8   p a r a   s e r   u s a d o   e m   s e g u i d a 
 
         a d d   r 6 ,   r 8 ,   r 2   @   s o m a   o   i n p u t   a o   o p e r a n d o   a n t e r i o r 
 
         b   p r i n t n u m b e r 
 
 p r e s s e d o p e r a t o r : 
 
         @   c h e c k i n g   w h i c h   s i g n   t o   a p p l y 
         c m p   r 0   ,   # P L U S _ K E Y 
         l d r e q   r 2 ,   = p l u s 
         c m p   r 0   ,   # M I N U S _ K E Y 
         l d r e q   r 2 ,   = m i n u s 
         c m p   r 0   ,   # M U L _ K E Y 
         l d r e q   r 2 ,   = m u l t i 
         c m p   r 0   ,   # D I V _ K E Y 
         l d r e q   r 2 ,   = d i v i 
         @   e n d   s i g n   c h e c k 
 
         a d d   r 4 ,   r 4 ,   # 1 
         m o v   r 0 ,   r 4 
         a d d   r 4 ,   r 4 ,   # 2 
 
         s w i   0 x 2 0 4 
         b   w a i t i n p u t 
 
 p r e s s e d e n t e r : 
         a d d   r 4 ,   r 4 ,   # 1 
         m o v   r 0 ,   # L E F T _ L E D   ; ; t m p 
         s w i   0 x 2 0 1 
         b   w a i t i n p u t 
 
 p r i n t n u m b e r : 
         m o v   r 0 ,   r 4   @   a s s i g n i n g   t h e   p o s i t i o n   o f   t h e   p l a c h o l d e r 
         a d d   r 4 ,   r 4 ,   # 1   @   c h a n g i n g   t h e   p o s i t i o n   o f   t h e   p l a c h o l d e r 
         s w i   0 x 2 0 5 
         b   w a i t i n p u t 
 
 
 s p a c e :     . a s c i z   "   "   @ m a y b e   u s e l e s s 
 p l u s :       . a s c i z   " + " 
 m i n u s :     . a s c i z   " - " 
 m u l t i :     . a s c i z   " * " 
 d i v i :       . a s c i z   " / " 
 
 o p e r a n d s :   . s p a c e   8 * 3 2 
 
 