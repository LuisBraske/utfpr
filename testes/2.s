@ TODO:
; - variável posição X (r0)
; 

;;; TMP
.equ SEG_A, 0x80 
.equ SEG_B, 0x40 
.equ SEG_C, 0x20 
.equ SEG_D, 0x08
.equ SEG_E, 0x04
.equ SEG_F, 0x02
.equ SEG_G, 0x01

;;; MISC
.equ LEFT_LED, 0x02

;;; NUMBERS

.equ NUM_KEY_1, 0x01 @button(0)
.equ NUM_KEY_2, 0x02 @button(1)
.equ NUM_KEY_3, 0x04 @button(2)
.equ NUM_KEY_4, 0x10 @button(4)
.equ NUM_KEY_5, 0x20 @button(5)
.equ NUM_KEY_6, 0x40 @button(6)
.equ NUM_KEY_7, 1<<8 @button(8) - different way to set
.equ NUM_KEY_8, 1<<9 @button(9)
.equ NUM_KEY_9, 1<<10 @button(10)
.equ NUM_KEY_0,  1<<13 @button(13)


.equ NULL_KEY,   1<<12 @button(12)
.equ ENTER_KEY,  1<<14 @button(14)

;;; OPERATORS
.equ PLUS_KEY,  0x08 @ ADD
.equ MINUS_KEY, 0x80 @ SUBSTRACT
.equ MUL_KEY,   1<<11 @ MULTIPLY
.equ DIV_KEY,   1<<15 @ DIVIDE

_main: mov r4, #0 @ numbers placholder
mov r5, #0 @ operators placholder

waitinput: swi 0x203
    cmp r0, #0
    beq waitinput

    cmp r0, #PLUS_KEY
    cmpne r0, #MINUS_KEY
    cmpne r0, #MUL_KEY
    cmpne r0, #DIV_KEY
    beq pressedoperator

    cmp r0, #ENTER_KEY
    beq pressedenter

    cmp r0, #NULL_KEY
    beq waitinput @ if "NULL" key, ie 3.0, pressed, branch to input loop

    b pressednumber @ if none of the above, defaults to "number"


draw: ADD r0, r0, #1
    SWI 0x205
    B waitinput


pressednumber:
    cmp r0, #NUM_KEY_1
    beq num_pres_1

    cmp r0, #NUM_KEY_2
    beq num_pres_2

    cmp r0, #NUM_KEY_3
    beq num_pres_3

    cmp r0, #NUM_KEY_4
    beq num_pres_4

    cmp r0, #NUM_KEY_5
    beq num_pres_5

    cmp r0, #NUM_KEY_6
    beq num_pres_6

    cmp r0, #NUM_KEY_7
    beq num_pres_7

    cmp r0, #NUM_KEY_8
    beq num_pres_8

    cmp r0, #NUM_KEY_9
    beq num_pres_9

    cmp r0, #NUM_KEY_0
    beq num_pres_0

pressedoperator:

    add r4, r4, #1
    mov r0, r4
    add r4, r4, #2
    ldr r2, =vtnc

    swi 0x204
    b waitinput


pressedenter:
    ; add r4, r4, #4
    ; mov r0, r4
    ; ldr r2, =vsf
    ; SWI 0x204
    mov r0, #LEFT_LED
    swi 0x201
    b waitinput

printnumber:
    mov r0, r4 @ assigning the position of the placholder
    add r4, r4, #1 @ changing the position of the placholder
    swi 0x205
    b waitinput


num_pres_1:
    mov r2, #1 @output
    b printnumber

num_pres_2:
    mov r2, #2 @output
    b printnumber

num_pres_3:
    mov r2, #3 @output
    b printnumber

num_pres_4:
    mov r2, #4 @output
    b printnumber

num_pres_5:
    mov r2, #5 @output
    b printnumber

num_pres_6:
    mov r2, #6 @output
    b printnumber

num_pres_7:
    mov r2, #7 @output
    b printnumber

num_pres_8:
    mov r2, #8 @output
    b printnumber

num_pres_9:
    mov r2, #9 @output
    b printnumber

num_pres_0:
    mov r2, #0 @output
    b printnumber

space: .asciz " "
vsf: .asciz "vsf"
vtnc: .asciz "+"
