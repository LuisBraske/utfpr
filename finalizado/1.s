b main

operands: .space 8*4 @ espação de 8 operandos de 32 bits cada

;;; MISC
.equ LEFT_LED, 0x02 @ LED esquerdo
.equ LEFT_BUT, 0x02 @ botão de função esquerdo

;;; NÚMEROS
.equ NUM_KEY_1, 0x01    @ botão 1
.equ NUM_KEY_2, 0x02    @ botão 2
.equ NUM_KEY_3, 0x04    @ botão 3
.equ NUM_KEY_4, 0x10    @ botão 4
.equ NUM_KEY_5, 0x20    @ botão 5
.equ NUM_KEY_6, 0x40    @ botão 6
.equ NUM_KEY_7, 1<<8    @ botão 8
.equ NUM_KEY_8, 1<<9    @ botão 9
.equ NUM_KEY_9, 1<<10   @ botão 10
.equ NUM_KEY_0,  1<<13  @ botão 13

.equ NULL_KEY,   1<<12 @ botão 12 (não faz nada)
.equ ENTER_KEY,  1<<14 @ botão 14 ENTER

;;; OPERADORES
.equ PLUS_KEY,  0x08 @ adição
.equ MINUS_KEY, 0x80 @ subtração
.equ MUL_KEY,   1<<11 @ multiplicação
.equ DIV_KEY,   1<<15 @ divisão

main:   
    mov r4, #0 @ posição X do cursor geral da tela LCD
    mov r5, #0 @ posição Y do cursor geral da tela LCD/contador de elementos do vetor de operandos

    mov r7, #10 @ incrementador de casas decimais do operando

    ldr r9, =operands @ vetor de operandos
    mov r0, #0
    swi 0x201 @ apaga os LEDs
    swi 0x206 @ limpa o display
    ;; R6 e R8 são genéricos

waitinput:
    swi 0x203
    cmp r0, #0
    bne init

    swi 0x202
    cmp r0, #LEFT_BUT
    beq main

    b waitinput

init:
    cmp r0, #PLUS_KEY
    cmpne r0, #MINUS_KEY
    cmpne r0, #MUL_KEY
    cmpne r0, #DIV_KEY
    beq pressedoperator

    cmp r0, #ENTER_KEY
    beq pressedenter

    cmp r0, #NULL_KEY
    beq waitinput @ se a tecla "NULL", ie 3.0, foi pressionada, nada acontece

    b pressednumber @ se nenhum dos de cima, é um número

pressednumber:

    cmp r0, #NUM_KEY_1
    moveq r2, #1

    cmp r0, #NUM_KEY_2
    moveq r2, #2

    cmp r0, #NUM_KEY_3
    moveq r2, #3

    cmp r0, #NUM_KEY_4
    moveq r2, #4

    cmp r0, #NUM_KEY_5
    moveq r2, #5

    cmp r0, #NUM_KEY_6
    moveq r2, #6

    cmp r0, #NUM_KEY_7
    moveq r2, #7

    cmp r0, #NUM_KEY_8
    moveq r2, #8

    cmp r0, #NUM_KEY_9
    moveq r2, #9

    cmp r0, #NUM_KEY_0
    moveq r2, #0

    cmp r4, #0 @ se o cursor não houver saído da posição "0", não multiplica por 10 (R7)
    mulgt r8, r6, r7 @ multiplica o número do operando anterior por dez e guarda em R8 para ser usado em seguida

    add r6, r8, r2 @ soma o input ao operando anterior e guarda em R2 para ser impresso na tela de LCD

    b printnumber

pressedoperator:

    cmp r5, #2
    blt waitinput @ se só um operado está na pilha, nada acontece

    sub r9, r9, #4
    ldr r1, [r9] @pega o operando do topo da pilha
    sub r9, r9, #4
    ldr r2, [r9] @pega o segundo operando do topo da pilha

    @ checking which sign to apply
    cmp r0, #PLUS_KEY
    addeq r3, r2, r1

    cmp r0, #MINUS_KEY
    subeq r3, r2, r1

    cmp r0, #DIV_KEY
    beq divide

    cmp r0, #MUL_KEY
    muleq r3, r2, r1

    ;______________________________

    b printresult

divide:
    cmp r1, #0
    beq lighton

    sub r2,

    mov r2, #33
    b printresult

pressedenter:
    cmp r5, #8
    strlt r6, [r9]
    addlt r9, r9, #4
    beq lighton
    cmplt r4, #0
    addgt r5, r5, #1 @ incrementa 1 na posição Y do cursor na tela se a pilha for menor que 8 e a posição X do cursor for maior que 0

    mov R4, #0 @ redefine a posição X do cursor na tela
    mov R8, #0 @ redefine a posição Y do cursor na tela
    swi 0x201
    b waitinput

lighton:
    mov r0, #LEFT_LED
    swi 0x201
    b waitinput

printnumber:
    mov r0, r4 @ posição X do cursor na tela
    mov r1, r5 @ posição Y do cursor na tela
    add r4, r4, #1 @ changing the position of the placholder
    swi 0x205
    b waitinput

printresult:
    str r3, [r9]
    add r9, r9, #4
    mov r2, r3 @ o resultado é armazenado em r2 para ser mostrado na tela de LCD

    sub r5, r5, #1
    mov r0, r5
    swi 0x208

    cmp r0, #0
    subne r0, r0, #1
    swi 0x208

    mov r1, r0

    mov r0, #0 @ posição X do cursor na tela
    swi 0x205
    b waitinput
